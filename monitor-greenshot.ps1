﻿#Set App Name and desired setup filename.
$appName = "Greenshot"

#Force TLS 1.2
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

#########################
#FINDER
#########################
$root = Invoke-WebRequest "http://getgreenshot.org/downloads/"

$contentURL = $root.links | ?{$_.href -match "exe"}

$content = $contentURL.href
#########################
& C:\Winston\corescripts\WinstonLog.ps1 -logstring "$appName finder determines content location to be: $content" -packageName $appName -component "$appName;Finder" -type 1

#########################
#Monitor
#########################

#Call universal monitor
& C:\Winston\corescripts\WinstonLog.ps1 -logstring "Calling Universal Monitor.  Sending arguments: $appName; $content; $PSScriptRoot; $fileName; $uMonitorExtractName" -packageName $appName -component "$appName;Finder" -type 1
$return = & C:\winston\corescripts\UniversalMonitor.ps1 -appName $appName -contentLocation $content -jenkinsWorkspace $PSScriptRoot -filename $fileName -extractName $uMonitorExtractName

#Evaluate Universal Monitor return coding.
if($return -match "-1")
{
    & C:\Winston\corescripts\WinstonLog.ps1 -logstring "Universal Monitor returns 0; $appName production version appears good.  Forwarding exit code 0 to Jenkins." -packageName $appName -component "$appName;Monitor" -type 1
    exit 0
}
elseif($return -match "-2")
{
    & C:\Winston\corescripts\WinstonLog.ps1 -logstring "Universal Monitor returns 2; error while evaluating $appName.  Forwarding exit code 1 to Jenkins." -packageName "$appName" -component "$appName;Monitor" -type 3
    exit 1
}
else
{
    & C:\Winston\corescripts\WinstonLog.ps1 -logstring "Universal Monitor returns version $return; $appName should be built." -packageName "$appName" -component "$appName;Monitor" -type 2
    
    #Copy the content to !installRepository so it can be picked up by the builder.
    $contentRepo = "\\euc-sw.m.storage.umich.edu\euc-sw\EUC\!Installps1\!installerrepository\$appName\latest"
    & C:\Winston\corescripts\Copy-RepoContent.ps1 -appName $appName -fileName $filename -contentRepo $contentRepo

    #Submit build project request via URL.  URL is case-sensitive, note escaped quotes.
    $buildURL = "https://pennyworth.miserver.it.umich.edu/remote-magic/view/Windows%20Jobs/job/Windows/job/Builders/job/win-universalbuilder/buildWithParameters?token=MWS&appName=`"$appName`"&appVersion=`"$return`"&filename=`"$filename`""
    & C:\Winston\corescripts\WinstonLog.ps1 -logstring "Issuing request to Jenkins via URL: $buildURL" -packageName "$appName" -component "Monitor" -type 1
    $buildResponse = Invoke-WebRequest -Uri $buildURL -Method POST
    & C:\Winston\corescripts\WinstonLog.ps1 -logstring "Response from build URL post is: $($buildResponse.StatusCode) - $($buildResponse.StatusDescription)" -packageName "$appName" -component "$appName;Monitor" -type 1

    #If successful, forward exit code 0 to Jenkins; else, return 1...
    if($buildResponse.StatusCode -eq 201)
    {
        & C:\Winston\corescripts\WinstonLog.ps1 -logstring "Due to build project post reporting success, forwarding exit code 0 to Jenkins." -packageName "$appName" -component "$appName;Monitor" -type 1
        exit 0
    }
    else 
    {
        & C:\Winston\corescripts\WinstonLog.ps1 -logstring "Due to build project post reporting failure, forwarding exit code 1 to Jenkins." -packageName "$appName" -component "$appName;Monitor" -type 3
        exit 1
    }
}